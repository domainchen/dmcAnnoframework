#dmcAnnoframework

使用说明：
一.基于mysql数据库
1.创建数据库:dmcanno
2.使用mysql数据库客户端运行数据库文件:src\main\resources\sql\dmcanno.sql
3.根据实际情况修改数据库配置文件::src\main\resources\conf\domainchen.properties

二.本项目基于Maven创建的,可直接导入eclipse/myeclipse/idea 等开发工具

三.com.domainchen.customer 包为运行实例


四.com.domainchen.framework 包为基建框架
1.com.domainchen.framework.common:框架公共类所在包
2.com.domainchen.framework.constant:框架参数所在包
3.com.domainchen.framework.core：框架核心包
4.com.domainchen.framework.dispatcther:基于Servlet 的前端控制器所在包

五.请求方式
1.支持基于表单提交请求
2.支持AJAX请求