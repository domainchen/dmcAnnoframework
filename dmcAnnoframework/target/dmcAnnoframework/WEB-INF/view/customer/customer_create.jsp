<%@ page pageEncoding="UTF-8"%>
<html>
<head>
<title>客户管理 - 创建客户</title>
</head>
<body>

	<h1>
		<a href="${BASE}/customer">客户管理</a> / 创建客户
	</h1>

	<form id="customer_form" method="post" action="${BASE}/customer_create">
		<table>
			<tr>
				<td>客户名称：</td>
				<td><input type="text" name="cus_name" id="cus_name"
					value="${customer.cus_name}"></td>
			</tr>
			<tr>
				<td>联系人：</td>
				<td><input type="text" name="contact" id="contact"
					value="${customer.contact}"></td>
			</tr>
			<tr>
				<td>电话号码：</td>
				<td><input type="text" name="telephone" id="telephone"
					value="${customer.telephone}"></td>
			</tr>
			<tr>
				<td>邮箱地址：</td>
				<td><input type="text" name="email" id="email"
					value="${customer.email}"></td>
			</tr>
		</table>
		<input type="button" onclick="addCustomer()">保存
		</button>
	</form>

	<script src="${BASE}/asset/lib/jquery/jquery.js"></script>
	<script src="${BASE}/asset/lib/jquery-form/jquery.form.min.js"></script>
	
</body>
</html>