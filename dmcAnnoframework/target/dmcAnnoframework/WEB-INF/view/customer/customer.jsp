<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	/* 	String path = request.getContextPath();
		String basePath = request.getScheme() + "://"
				+ request.getServerName() + ":" + request.getServerPort()
				+ path + "/"; */
%>
<c:set var="BASE" value="${pageContext.request.contextPath} }" />
<html>
<head>
<%-- <base href="<%=basePath%>" --%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>客户管理-客户列表</title>
<style type="text/css">
table{
 
}

</style>
</head>
<body>
	<h1> 客户列表</h1>
	<h2><a href="customer_create">创建客户</a></h2>
	<table border="1px sold" style="text-align: center">
		<thead>
			<tr>
				<th>编号</th>
				<th>客户名称</th>
				<th>联系人</th>
				<th>电话号码</th>
				<th>邮箱地址</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="customer" items="${customerList}">
				<tr>
                  <td>${customer.id}</td>
                  <td>${customer.cus_name}</td>
                  <td>${customer.contact}</td>
                  <td>${customer.telephone}</td>
                  <td>${customer.email}</td>
                  <td>
                    <a href="customer_edit?id=${customer.id}">编辑</a>
                    <a href="customer_delete?id=${customer.id}">删除</a>
                  </td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>