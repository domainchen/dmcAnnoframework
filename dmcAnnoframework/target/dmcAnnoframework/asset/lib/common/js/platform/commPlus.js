(function(window, document, undefined) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
	"use strict";
	var dd;
    $.shanhu = {
		cfg: {
        	pageSize:7
        },
        ajaxForOper:function(opts){
			if(!opts.hideTip){
				$.shanhu.showLoading(); 
			}
    		var settings = {
    			url:"",
    			data:"",
    			type:"POST",
    			dataType:"json",
    			sucMsgIsShow:true,
    		    callback:function(){return false;}
        	};
    		opts = jQuery.extend(settings,opts);
    		var typeStr =  opts.type;
    		var urlStr = opts.url;
    		var dataTypeStr = opts.dataType;
    		var dataStr = opts.data;
    		$.ajax({
    			type:typeStr,
    		    url:urlStr,
    		    dataType:dataTypeStr, 
    		    data:dataStr,
    		    cache:false,
    		    timeout: 10000,  		  
    		    complete: function(XMLHttpRequest,textStatus){
 		 		   if(XMLHttpRequest.status == 535){
  		 			  $.shanhu.showErrMes("请先登陆！","登陆信息");
  		 		   }
  		 	   },
  		 	   success:function(data){
  		      		 opts.callback(data);
    		       	 if(data.isSuc == "NO"){
    		       		$.shanhu.showErrMes(data.errMes, "错误信息", data.redirectUrl);
    		       		$.shanhu.hideLoading();
    		       	 }else{
    		       		 $.shanhu.hideLoading();
    		       		 if(null != data && null != data.sucMes && opts.sucMsgIsShow){
    		       			$.shanhu.showSucMes(data.sucMes, "成功信息",data.redirectUrl);
    		       		 }
    		       	 }
    		   },
 		 	   error:function(XMLHttpRequest,textStatus) {
 		 	   	   if(XMLHttpRequest.status != 535 && (XMLHttpRequest.status != 404)){
 		 	   		  $.shanhu.showErrMes("网络不稳定，请重新加载！");
 		 		   }
 		 		  $.shanhu.hideLoading();
 			   }
    		});
    	 }
     };
})(window, document);