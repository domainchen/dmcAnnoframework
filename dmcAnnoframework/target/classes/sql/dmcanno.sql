/*
Navicat MySQL Data Transfer

Source Server         : dev_localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : dmcanno

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2016-08-10 21:55:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cus_name` varchar(255) NOT NULL COMMENT '客户名称',
  `contact` varchar(255) NOT NULL COMMENT '联系人',
  `telephone` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(255) DEFAULT NULL COMMENT 'EMAIL',
  `remark` text COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES ('1', 'domainchen', '18030309793', '18030309793', 'cytxiamen@163.com', '黄埔');
INSERT INTO `customer` VALUES ('2', 'sd', 'fsdf', 'sdf', 'sdf', null);
INSERT INTO `customer` VALUES ('3', 'c', 'c', 'c', 'ffff', null);
