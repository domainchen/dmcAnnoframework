package com.domainchen.customer.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.domainchen.customer.model.Customer;
import com.domainchen.customer.service.CustomerService;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * 
 * 对于测试类中方法的要求： 在JUnit 3.8中，测试方法需要满足如下原则： 
 * 1.public的。 
 * 2.void的。 
 * 3.无方法参数。
 * 4.方法名称必须以test开头。 （它通过反射找出所有方法，然后找出以test开头的方法）。 Test
 * Case之间一定要保持完全的独立性，不允许出现任何的依赖关系。 删除一些方法后不会对其他的方法产生任何的影响。 我们不能依赖于测试方法的执行顺序。
 * @在每个测试用例之前执行setUp()，每个测试用例执行之后，tearDown()会执行
 * 
 * @author 跃腾
 *
 */
public class CustomerServiceTest  {

	private static CustomerService customerService;

	static{
		customerService = new CustomerService();
	}
	/**
	 * 每个测试方法执行前 执行
	 */
//	@Override
//	protected void setUp() throws Exception {
//		// TODO Auto-generated method stub
//		super.setUp();
//		customerService = new CustomerService();
//	}

	/**
	 * 每个测试方法执行后 执行
	 */
//	@Override
//	protected void tearDown() throws Exception {
//		// TODO Auto-generated method stub
//		super.tearDown();
//	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	public static void testGetCustomerList() {
		String filed = "";
		List<Customer> customerList = customerService.getCustomerList();
		Assert.assertEquals(2, customerList.size());
	}

	/**
	 * 获取客户
	 * 
	 * @param id
	 */
	public static void testGetCustomer() {
		int id = 1;
		Customer customer = customerService.getCustomer(id);
		Assert.assertNotNull(customer);
	}

	/**
	 * 创建客户
	 * 
	 * @param filedMap
	 * @return
	 */
	public static void testCreateCustomer() {
		Map<String, Object> filedMap = new HashMap<String, Object>();
		filedMap.put("cus_name", "domainchen003");
		filedMap.put("contact", "18046307075");
		filedMap.put("telephone", "18046307075");
		filedMap.put("email", "1010602131@qq.com");
		filedMap.put("remark", "黄埔");
		boolean result = customerService.createCustomer(filedMap);
		Assert.assertTrue(result);

	}

	/**
	 * 更新客户
	 * 
	 * @param filedMap
	 * @return
	 */
	public static void testUpdateCustomer(){
		Map<String, Object> filedMap = new HashMap<String, Object>();
	//	filedMap.put("id", 3);
		filedMap.put("cus_name", "domainchen003");
		filedMap.put("contact", "18046307075");
		filedMap.put("telephone", "18046307075");
		filedMap.put("email", "1010602131@qq.com");
		filedMap.put("remark", "黄埔");
		boolean result = customerService.updateCustomer(3,filedMap);
		Assert.assertTrue(result);
	}

	/**
	 * 删除客户
	 * 
	 * @param id
	 * @return
	 */
	public static void testDeleteCustomer() {
		Integer id = 3;
		boolean result = customerService.deleteCustomer(id);
		Assert.assertTrue(result);
	}
	
	public static void main(String[] args) {
		CustomerServiceTest.testGetCustomerList();
		//testGetCustomer();
		//testCreateCustomer();
		//testUpdateCustomer();
		//testDeleteCustomer();
	}
}
