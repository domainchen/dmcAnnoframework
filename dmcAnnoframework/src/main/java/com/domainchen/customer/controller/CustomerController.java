package com.domainchen.customer.controller;

import java.util.List;
import java.util.Map;

import com.domainchen.customer.model.Customer;
import com.domainchen.customer.service.CustomerService;
import com.domainchen.framework.core.annotation.Action;
import com.domainchen.framework.core.annotation.Controller;
import com.domainchen.framework.core.annotation.Inject;
import com.domainchen.framework.core.bean.Param;
import com.domainchen.framework.core.bean.RtnData;
import com.domainchen.framework.core.bean.ViewModel;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年8月06日 下午3:01:27
 * @类说明 客户管理
 */
@Controller
public class CustomerController {

	@Inject
	private CustomerService customerService;

	/**
	 * 进入 客户列表
	 * 
	 * @param param
	 * @return
	 */
	@Action("get:/customer")
	public ViewModel index() {
		List<Customer> customerList = customerService.getCustomerList();
		return new ViewModel("customer/customer.jsp").addModel("customerList", customerList);
	}

	/**
	 * 显示客户基本信息
	 * 
	 * @param param
	 * @return
	 */
	@Action("get:/customer_show")
	public ViewModel show(Param param) {
		int id = param.getInt("id");
		Customer customer = customerService.getCustomer(id);
		return new ViewModel("customer/customer_show.jsp").addModel("customer", customer);
	}

	/**
	 * 进入 创建客户 页面
	 * 
	 * @param param
	 * @return
	 */
	@Action("get:/customer_create")
	public ViewModel create() {
		return new ViewModel("customer/customer_create.jsp");
	}

	/**
	 * 处理创建客户请求
	 * 
	 * @param param
	 * @return
	 */
	@Action("post:/customer_create")
	public ViewModel createSubmit(Param param) {
		Map<String, Object> filedMap = param.getFieldMap();
		boolean result = customerService.createCustomer(filedMap);
		
		List<Customer> customerList = customerService.getCustomerList();
		return new ViewModel("customer/customer.jsp").addModel("customerList", customerList);
	}

	/**
	 * 进入 编辑客户 界面
	 * 
	 * @param param
	 * @return
	 */
	@Action("get:/customer_edit")
	public ViewModel edit(Param param) {
		int id = param.getInt("id");
		Customer customer = customerService.getCustomer(id);
		return new ViewModel("customer/customer_edit.jsp").addModel("customer", customer);
	}

	/**
	 * 处理 编辑客户 请求
	 * 
	 * @param param
	 * @return
	 */
	@Action("put:/customer_edit")
	public RtnData editSubmit(Param param) {
		int id = param.getInt("id");
		Map<String, Object> filedMap = param.getFieldMap();
		boolean result = customerService.updateCustomer(id, filedMap);
		return new RtnData(result);
	}

	@Action("get:/customer_delete")
	public ViewModel delete(Param param) {
		int id = param.getInt("id");
		customerService.deleteCustomer(id);

		List<Customer> customerList = customerService.getCustomerList();
		return new ViewModel("customer/customer.jsp").addModel("customerList", customerList);

	}
}
