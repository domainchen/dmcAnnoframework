package com.domainchen.customer.model;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年8月06日 下午2:23:18
 * @类说明 客户表
 */
public class Customer {

	private Integer id;// 主键
	private String cus_name;// 客户名称
	private String contact;// 联系人
	private String telephone;// 联系电话
	private String email;// email
	private String remark;// 备注

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCus_name() {
		return cus_name;
	}

	public void setCus_name(String cus_name) {
		this.cus_name = cus_name;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
