package com.domainchen.customer.service;

import java.util.List;
import java.util.Map;

import com.domainchen.customer.model.Customer;
import com.domainchen.framework.common.persistence.DataBaseHepper;
import com.domainchen.framework.core.annotation.Service;
import com.domainchen.framework.core.annotation.Transaction;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年8月06日 下午2:13:24
 * @类说明 客户服务类
 */
@Service
public class CustomerService {

	/**
	 * 获取客户列表
	 * 
	 * @param keywork
	 * @return
	 */
	public List<Customer> getCustomerList() {
		String sql="SELECT * FROM Customer";
		return DataBaseHepper.queryEnityList(Customer.class,sql);
	}

	/**
	 * 获取客户
	 * 
	 * @param id
	 */
	public Customer getCustomer(int id) {
		String sql="SELECT * FROM Customer WHERE id=?";
		Customer customer=DataBaseHepper.queryEntity(Customer.class,sql, id);
		return customer;
	}

	/**
	 * 创建客户
	 * 
	 * @param filedMap
	 * @return
	 */
	@Transaction
	public boolean createCustomer(Map<String, Object> filedMap) {
		return DataBaseHepper.insertEntity(Customer.class, filedMap);
	}

	/**
	 * 更新客户
	 * 
	 * @param filedMap
	 * @return
	 */
	@Transaction
	public boolean updateCustomer(int id,Map<String, Object> filedMap) {
		return DataBaseHepper.updateEntity(Customer.class, id, filedMap);
	}

	/**
	 * 删除客户
	 * @param id
	 * @return
	 */
	@Transaction
	public boolean deleteCustomer(int id) {
		return DataBaseHepper.deleteEntity(Customer.class, id);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
