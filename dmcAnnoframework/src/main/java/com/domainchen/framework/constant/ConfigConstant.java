package com.domainchen.framework.constant;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月10日 下午3:34:12
 * @类说明 提供相关配置项常量
 */
public interface ConfigConstant {

	String CONFIG_FILE = "conf/domainchen.properties";

	String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	String JDBC_URL = "domainchen.framework.connection.url";
	String JDBC_USERNAME = "domainchen.framework.connection.username";
	String JDBC_PASSWORD = "domainchen.framework.connection.password";

	String APP_BASE_PACKAGE = "domainchen.framework.app.base_package";
	String APP_JSP_PATH = "domainchen.framework.app.jsp_path";
	String APP_DEFAULT_PATH = "domainchen.framework.app.default_path";
	String APP_ASSERT_PATH = "domainchen.framework.app.assert_path";
	
	
	String APP_UPLOAD_LIMIT = "domainchen.framework.app.upload_limit";//文件限制大小
}
