package com.domainchen.framework.core.util;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月15日 下午9:53:47
 * @类说明
 */
public class ServletHolder {

	private static Logger logger = LoggerFactory.getLogger(ServletHolder.class);

	/**
	 * 使每个线程独立拥有一份 ServletHolder 实例
	 */
	private static final ThreadLocal<ServletHolder> SERVLET_HOLDER = new ThreadLocal<ServletHolder>();

	private HttpServletRequest request;
	private HttpServletResponse response;

	public ServletHolder(HttpServletRequest request, HttpServletResponse response) {
		super();
		this.request = request;
		this.response = response;
	}

	/**
	 * 初始化
	 * 
	 * @param request
	 * @param response
	 */
	public static void init(HttpServletRequest request, HttpServletResponse response) {
		SERVLET_HOLDER.set(new ServletHolder(request, response));
	}

	/**
	 * 销毁
	 */
	public static void destory() {
		SERVLET_HOLDER.remove();
	}

	/**
	 * 获取 Request 对象
	 * 
	 * @return
	 */
	private static HttpServletRequest getRequest() {
		return SERVLET_HOLDER.get().request;
	}

	/**
	 * 获取 Response 对象
	 * 
	 * @return
	 */
	private static HttpServletResponse getResponse() {
		return SERVLET_HOLDER.get().response;
	}

	/**
	 * 获取 Session 对象
	 * 
	 * @return
	 */
	private static HttpSession getSession() {
		return getRequest().getSession();
	}

	/**
	 * 获取 ServletContext 对象
	 * 
	 * @return
	 */
	private static ServletContext getServletContext() {
		return getRequest().getSession().getServletContext();
	}

	///////////////////////////////////////////////////////////////////////////////////////
	/**
	 * 将属性放入 Request
	 * 
	 * @param key
	 * @param value
	 */
	public static void setRequestAttribute(String key, Object value) {
		getRequest().setAttribute(key, value);
	}

	/**
	 * 从 Request 中获取 属性
	 * 
	 * @param key
	 * @return
	 */
	public static <T> T getRequestAttribute(String key) {
		return (T) getRequest().getAttribute(key);
	}

	/**
	 * 从 Request 中 移除属性
	 * 
	 * @param key
	 */
	public static void removeRequestAttribute(String key) {
		getRequest().removeAttribute(key);
	}

	/**
	 * 发送重定向
	 * 
	 * @param location
	 */
	public static void sendRedirect(String location) {
		try {
			getResponse().sendRedirect(location);
		} catch (IOException e) {
			logger.error(" redirect faulure", e);
			e.printStackTrace();
		}
	}

	/**
	 * 将 属性 放入Session 中
	 * 
	 * @param key
	 * @param value
	 */
	public static void setSessionAttribute(String key, Object value) {
		getSession().setAttribute(key, value);
	}

	/**
	 * 从 Session 中 获取属性
	 * 
	 * @param key
	 * @return
	 */
	public static <T> T getSessionAttribute(String key) {
		return (T) getSession().getAttribute(key);
	}

	/**
	 * 将 属性从 Session 中移除
	 * 
	 * @param key
	 */
	public static void removeSessionAttribute(String key) {
		getSession().removeAttribute(key);
	}

	/**
	 * 使 Session 失效
	 */
	public static void invalidateSession() {
		getRequest().getSession().invalidate();
	}
}
