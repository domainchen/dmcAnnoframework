package com.domainchen.framework.core.proxy;

import java.lang.reflect.Method;
import java.util.List;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月11日 下午5:34:31
 * @类说明 代理管理器
 */
public class ProxyManager {

	@SuppressWarnings("unchecked")
	public static <T> T createProxy(final Class<?> targetClass, final List<Proxy> proxyList) {
		return (T) Enhancer.create(targetClass, new MethodInterceptor() {

			@Override
			public Object intercept(Object targetObject, Method targetMethod, Object[] methodParams,
					MethodProxy methodProxy) throws Throwable {
				// TODO Auto-generated method stub

				return new ProxyChain(targetClass,targetObject, targetMethod, methodProxy, methodParams, proxyList).doProxyChain();
			}
		});
	}
}
