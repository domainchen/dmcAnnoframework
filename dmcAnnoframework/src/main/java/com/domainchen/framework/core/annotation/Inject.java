package com.domainchen.framework.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
* @author  domainchen or cytxiamen@163.com
* @version 创建时间：2016年3月10日 下午4:46:18 
* @类说明  依赖注入的注解
*/
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {

}
