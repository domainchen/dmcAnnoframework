package com.domainchen.framework.core.holder;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.domainchen.framework.common.util.ArrayUtil;
import com.domainchen.framework.common.util.CollectionUtil;
import com.domainchen.framework.core.annotation.Action;
import com.domainchen.framework.core.bean.Handler;
import com.domainchen.framework.core.bean.Request;

import junit.framework.Assert;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月11日 上午1:16:52
 * @类说明 控制器助手类
 */
public final class ControllerHolder {

	private static final Map<Request, Handler> ACTION_MAP = new HashMap<Request, Handler>();

	static {
		Set<Class<?>> controllerClassSet = ClassHolder.getControllerClassSet();
		
		Assert.assertNotNull(controllerClassSet);
		
		if (CollectionUtil.isNotEmpty(controllerClassSet)) {
			// 遍历这些 Controller
			System.out.println("======================================================================================");
			for (Class<?> controllerClass : controllerClassSet) {
				// 获取Controller 类中定义的方法
				Method[] methods = controllerClass.getDeclaredMethods();
				if (ArrayUtil.isNotEmpty(methods)) {
					// 遍历这些 Controller 中的方法
					for (Method method : methods) {
						// 判断当前方法是否带有Action注解
						if (method.isAnnotationPresent(Action.class)) {
							// 从Action 注解中获取URL映射规则
							Action action = method.getAnnotation(Action.class);
							String mapping = action.value();
							// 验证URL规则
							if (mapping.matches("\\w+:/\\w*")) {
								String[] array = mapping.split(":");
								if (ArrayUtil.isNotEmpty(array) && array.length == 2) {
									// 获取请求方法与请求路径
									String requestMethod = array[0];
									String requestPath = array[1];
									Request request = new Request(requestMethod, requestPath);
									Handler handler = new Handler(controllerClass, method);
									System.out.println("[请求映射]:"+requestPath+"====>"+method.getName());
									// 初始化ACTION_MAP
									ACTION_MAP.put(request, handler);
								}
							}
						}
					}
				}
			}
		}
	}

	public static Handler getHandler(String requestMethod, String requestPath) {
		Request request = new Request(requestMethod, requestPath);
		return ACTION_MAP.get(request);
	}
}
