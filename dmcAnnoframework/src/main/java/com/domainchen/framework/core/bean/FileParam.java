package com.domainchen.framework.core.bean;

import java.io.InputStream;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月15日 下午5:31:49
 * @类说明 封装上传文件参数
 */
public class FileParam {

	private String fieldName;// 表单字段名

	private String fileName;// 上传的文件的文件名

	private long fileSize;// 文件大小

	private String contentType;// 上传文件的content-Type,可判断文件类型

	private InputStream inputStream;// 上传文件的直接流

	public FileParam(String fieldName, String fileName, long fileSize, String contentType, InputStream inputStream) {
		super();
		this.fieldName = fieldName;
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.contentType = contentType;
		this.inputStream = inputStream;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getFileName() {
		return fileName;
	}

	public long getFileSize() {
		return fileSize;
	}

	public String getContentType() {
		return contentType;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

}
