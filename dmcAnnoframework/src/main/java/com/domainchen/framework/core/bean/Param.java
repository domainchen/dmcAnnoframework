package com.domainchen.framework.core.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.domainchen.framework.common.util.CastUtil;
import com.domainchen.framework.common.util.CollectionUtil;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月11日 上午9:17:17
 * @类说明 请求参数对象
 */
public class Param {

	// private Map<String, Object> paramMap = null;

	private List<FormParam> formParamList;

	private List<FileParam> fileParamList;

	//
	// public Param(Map<String, Object> paramMap) {
	// this.paramMap = paramMap;
	// }

	public Param(List<FormParam> formParamList) {
		this.formParamList = formParamList;
	}

	public Param(List<FormParam> formParamList, List<FileParam> fileParamList) {
		this.formParamList = formParamList;
		this.fileParamList = fileParamList;
	}

	/**
	 * 获取请求参数映射
	 * 
	 * @return
	 */
	public Map<String, Object> getFieldMap() {
		Map<String, Object> fieldMap = new HashMap<String, Object>();
		if (CollectionUtil.isNotEmpty(formParamList)) {
			for (FormParam formParam : formParamList) {
				String fieldName = formParam.getFieldName();
				Object fieldValue = formParam.getFieldValue();
				if (fieldMap.containsKey(fieldName)) {
					fieldValue = fieldMap.get(fieldName) + com.domainchen.framework.common.util.StringUtils.SEPARATOR
							+ fieldValue;
				}
				fieldMap.put(fieldName, fieldValue);
			}
		}
		return fieldMap;
	}

	/**
	 * 获取上传文件映射
	 * 
	 * @return
	 */
	public Map<String, List<FileParam>> getFileMap() {
		Map<String, List<FileParam>> fileMap = new HashMap<String, List<FileParam>>();
		if (CollectionUtil.isNotEmpty(fileParamList)) {
			for (FileParam fileParam : fileParamList) {
				String fileName = fileParam.getFieldName();
				List<FileParam> fileParamList = null;
				if (fileMap.containsKey(fileName)) {
					fileParamList = fileMap.get(fileName);
				} else {
					fileParamList = new ArrayList<FileParam>();
				}
				fileParamList.add(fileParam);
				fileMap.put(fileName, fileParamList);
			}
		}
		return fileMap;
	}

	/**
	 * 获取所有上传文件
	 * 
	 * @param fieldName
	 * @return
	 */
	public List<FileParam> getFileList(String fieldName) {
		return getFileMap().get(fieldName);
	}

	/**
	 * 获取单个上传文件
	 * 
	 * @param fieldName
	 * @return
	 */
	public FileParam getFileParam(String fieldName) {
		List<FileParam> fileParams = getFileMap().get(fieldName);
		if (CollectionUtil.isNotEmpty(fileParamList) && fileParams.size() == 1) {
			return fileParams.get(0);
		}
		return null;
	}

	public boolean isEmpty() {
		return CollectionUtil.isEmpty(formParamList) && CollectionUtil.isEmpty(fileParamList);
	}

	/**
	 * 根据 参数 获取 Int型
	 * 
	 * @param name
	 * @return
	 */
	public int getInt(String name) {
		return CastUtil.castInt(getFieldMap().get(name));
	}

	/**
	 * 根据 参数 获取 Long型
	 * 
	 * @param name
	 * @return
	 */
	public long getLong(String name) {
		return CastUtil.castLong(getFieldMap().get(name));
	}

	public boolean getBoolean(String name) {
		return CastUtil.castBoolean(getFieldMap().get(name));
	}
	/**
	 * 获取所有参数
	 * 
	 * @return
	 */
	// public Map<String, Object> getParamMap() {
	// return paramMap;
	// }
}
