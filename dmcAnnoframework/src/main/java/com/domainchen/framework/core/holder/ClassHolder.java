package com.domainchen.framework.core.holder;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.domainchen.framework.common.config.Global;
import com.domainchen.framework.core.annotation.Controller;
import com.domainchen.framework.core.annotation.Service;

import junit.framework.Assert;


/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月10日 下午5:13:30
 * @类说明 类操作助手类
 */
public final class ClassHolder {

	/**
	 * 定义类集合(用于存放所加载的类)
	 */
	private static Set<Class<?>> CLASS_SET = null;

	static {
		String basePackage = Global.getAppBasePackage();
		System.out.println("ClassHolder【扫描包】:"+basePackage);
		CLASS_SET = com.domainchen.framework.core.util.ClassLoader.getClassSet(basePackage);
	}

	/**
	 * 获取应用包名下的所有类
	 * 
	 * @return
	 */
	public static Set<Class<?>> getClassSet() {
		return CLASS_SET;
	}

	/**
	 * 获取应用包名下的所有Service类
	 * 
	 * @return
	 */
	public static Set<Class<?>> getServiceClassSet() {
		Set<Class<?>> serviceClassSet = new HashSet<Class<?>>();
		for (Class<?> clas : CLASS_SET) {
			if (clas.isAnnotationPresent(Service.class)) {
				serviceClassSet.add(clas);
			}
		}
		return serviceClassSet;
	}

	/**
	 * 获取应用包名下的所有Controller类
	 * 
	 * @return
	 */
	public static Set<Class<?>> getControllerClassSet() {
		Set<Class<?>> controllerClassSet = new HashSet<Class<?>>();
		for (Class<?> clas : CLASS_SET) {
			if (clas.isAnnotationPresent(Controller.class)) {
				controllerClassSet.add(clas);
			}
		}
		return controllerClassSet;
	}

	/**
	 * 获取应用包名下的所有Bean(包含Service类/Controller类)
	 * 
	 * @return
	 */
	public static Set<Class<?>> getBeanClassSet() {
		Set<Class<?>> beanClassSet = new HashSet<Class<?>>();
		beanClassSet.addAll(getServiceClassSet());
		beanClassSet.addAll(getControllerClassSet());
		return beanClassSet;
	}

	/**
	 * 获取应用包名下带有注解的所有类
	 * 
	 * @param superClass
	 * @return
	 */
	public static Set<Class<?>> getClassSetBySuper(Class<?> superClass) {
		Set<Class<?>> classSet = new HashSet<Class<?>>();
		for (Class<?> cls : CLASS_SET) {
			if (superClass.isAssignableFrom(cls) && superClass.equals(cls)) {
				classSet.add(cls);
			}
		}
		return classSet;
	}

	/**
	 * 获取应用包名下带有某注解的所有类
	 * 
	 * @return
	 */
	public static Set<Class<?>> getClassSetByAnnotation(Class<? extends Annotation> annotationClass) {
		Set<Class<?>> classSet = new HashSet<Class<?>>();
		for (Class<?> cls : CLASS_SET) {
			if (cls.isAnnotationPresent(annotationClass)) {
				classSet.add(cls);
			}
		}
		return classSet;
	}
}
