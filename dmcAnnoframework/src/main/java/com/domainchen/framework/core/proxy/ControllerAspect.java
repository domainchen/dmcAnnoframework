package com.domainchen.framework.core.proxy;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.domainchen.framework.core.annotation.Aspect;
import com.domainchen.framework.core.annotation.Controller;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月11日 下午5:57:58
 * @类说明 拦截Controller 方法
 */
@Aspect(Controller.class)
public class ControllerAspect extends AspectProxy {

	private static Logger logger = LoggerFactory.getLogger(ControllerAspect.class);

	private long begin;

	@Override
	public void before(Class<?> cls, Method method, Object[] params) throws Throwable {
		// TODO Auto-generated method stub
		logger.debug("-----------------begin-----------------------------");
		logger.debug(String.format("class :%s", cls.getName()));
		logger.debug(String.format("method :%s", method.getName()));
		begin = System.currentTimeMillis();
	}

	
	public void after(Class<?> cls, Method method, Object[] params, Object result) throws Throwable {
		logger.debug(String.format("time :%dms", System.currentTimeMillis()-begin));
		logger.debug("-----------------end-----------------------------");
	}
}
