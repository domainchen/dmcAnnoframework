package com.domainchen.framework.core.util.classUtil;

import com.domainchen.framework.core.holder.AopHolder;
import com.domainchen.framework.core.holder.BeanHolder;
import com.domainchen.framework.core.holder.ClassHolder;
import com.domainchen.framework.core.holder.ControllerHolder;
import com.domainchen.framework.core.holder.IocHolder;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月11日 上午1:51:33
 * @类说明 初始化框架 --加载Holder
 */
public final class HolderLoader {

	// AopHolder 要在 IocHolder之前: 需通过AopHolder获取代理对象,然后才能通过IocHolder进行依赖注入
	public static void init() {
		Class<?>[] classList = { ClassHolder.class, // 所有类加载
				BeanHolder.class, // bean加载
				AopHolder.class, // Aop加载
				IocHolder.class, // DI/IOC 加载
				ControllerHolder.class };
		for (Class<?> cla : classList) {
			com.domainchen.framework.core.util.ClassLoader.loadClass(cla.getName(), true);
		}
	}
}
