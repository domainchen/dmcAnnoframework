package com.domainchen.framework.core.bean;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月11日 上午9:59:08
 * @类说明 返回数据对象
 * @之后转Json
 */
public class RtnData {

	/**
	 *
	 */
	private String isSuc;

	/**
	 * 返回成功信息
	 */
	private String sucMes;//

	/**
	 * 返回错误信息
	 */
	private String errMes;//

	/**
	 * 模型数据
	 */
	private Object model;

	public RtnData(Object model) {
		this.isSuc = "YES";
		this.model = model;
	}

	public Object getModel() {
		return model;
	}

	public void setModel(Object model) {
		this.model = model;
	}

	public String getIsSuc() {
		return isSuc;
	}

	public void setIsSuc(String isSuc) {
		this.isSuc = isSuc;
	}

	public String getSucMes() {
		return sucMes;
	}

	public void setSucMes(String sucMes) {
		this.sucMes = sucMes;
	}

	public String getErrMes() {
		return errMes;
	}

	public void setErrMes(String errMes) {
		this.errMes = errMes;
	}

}
