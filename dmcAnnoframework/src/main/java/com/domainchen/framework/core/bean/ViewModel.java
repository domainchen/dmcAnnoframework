package com.domainchen.framework.core.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月11日 上午9:26:40
 * @类说明 返回视图对象
 */
public class ViewModel {

	/**
	 * 视图路径
	 */
	private String viewPath;

	/**
	 * 模型数据
	 */
	private Map<String, Object> model;

	public ViewModel(String viewPath) {
		this.viewPath = viewPath;
		model = new HashMap<String, Object>();
	}

	public ViewModel addModel(String key, Object value) {
		model.put(key, value);
		return this;
	}

	public String getViewPath() {
		return viewPath;
	}

	public Map<String, Object> getModel() {
		return model;
	}

}
