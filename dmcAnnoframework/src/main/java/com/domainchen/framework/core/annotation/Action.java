package com.domainchen.framework.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月10日 下午4:42:15
 * @类说明  方法注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Action {

	String value();// 请求类型与路径
}
