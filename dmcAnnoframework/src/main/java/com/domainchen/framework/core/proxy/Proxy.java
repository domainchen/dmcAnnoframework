package com.domainchen.framework.core.proxy;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月11日 下午5:15:24
 * @类说明 代理接口
 */
public interface Proxy {

	/**
	 * 执行链式代理
	 * 
	 * @param proxyChain
	 * @return
	 * @throws Throwable
	 */
	Object doProxy(ProxyChain proxyChain) throws Throwable;
}
