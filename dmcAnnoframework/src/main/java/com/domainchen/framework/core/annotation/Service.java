package com.domainchen.framework.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月10日 下午4:44:12
 * @类说明   服务类主键
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Service {

}
