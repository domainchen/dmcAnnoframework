package com.domainchen.framework.core.bean;

import java.lang.reflect.Method;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月11日 上午1:25:00
 * @类说明 封装Action信息
 */
public class Handler {

	// Controller 类
	private Class<?> controllerClass;
	// Action
	private Method actionMethod;

	public Handler(Class<?> controllerClass,Method actionMethod){
		this.controllerClass=controllerClass;
		this.actionMethod=actionMethod;
	}

	public Class<?> getControllerClass() {
		return controllerClass;
	}

	public Method getActionMethod() {
		return actionMethod;
	}
	
}
