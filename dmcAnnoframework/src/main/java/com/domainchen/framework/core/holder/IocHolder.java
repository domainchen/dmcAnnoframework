package com.domainchen.framework.core.holder;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.text.html.parser.Entity;

import org.apache.commons.lang3.StringUtils;

import com.domainchen.framework.common.util.ArrayUtil;
import com.domainchen.framework.common.util.CollectionUtil;
import com.domainchen.framework.core.util.Reflections;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月11日 上午12:53:18
 * @类说明 依赖注入类 DI/Inect
 */
public final class IocHolder {

	static {
		// 获取 所有Bean 类与Bean实例之间的映射关系(简称Bean Map)
		Map<Class<?>, Object> beanMap = BeanHolder.getBeanMap();
		if (CollectionUtil.isNotEmpty(beanMap)) {
			// 遍历 Bean Map
			System.out.println("======================================================================================");
			for (Entry<Class<?>, Object> beanEntry : beanMap.entrySet()) {
				// 从BeanMap 中获取Bean类与Bean实例
				Class<?> beanClass = beanEntry.getKey();
				Object beanInstance = beanEntry.getValue();
				// 获取Bean类定义的所有成员变量(简称 Bean Field)
				Field[] beanFields = beanClass.getDeclaredFields();
				if (ArrayUtil.isNotEmpty(beanFields)) {
					// 遍历 Bean Field
					for(Field beanField:beanFields){
						//判断当前 Bean Field 是否带有 Inject 注解
						Class<?>  beanFieldClass=beanField.getType();
						Object  beanFieldInstance=beanMap.get(beanFieldClass);
						System.out.println("[依赖注入类]:"+beanInstance.getClass().getName()+"\t"+beanField.getName()+"->"+beanFieldInstance);
						if(StringUtils.isNotEmpty(beanFieldInstance.toString())){
							//通过反射初始化BeanField的值
							
							Reflections.setFieldValue(beanInstance, beanField, beanFieldInstance);
							
						}
					}
				}
			}
		}
	}
	

}
