package com.domainchen.framework.common.util;

import org.apache.commons.lang3.ArrayUtils;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月11日 上午1:11:51
 * @类说明 数组工具类
 */
public final class ArrayUtil {

	/**
	 * 判断数组是否为空
	 * 
	 * @param array
	 * @return
	 */
	public static boolean isNotEmpty(Object[] array) {
		return !ArrayUtils.isEmpty(array);
	}

	public static boolean isEmpty(Object[] array) {
		return ArrayUtils.isEmpty(array);
	}
}
