package com.domainchen.framework.common.util;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月15日 下午8:50:22
 * @类说明 文件操作工具类
 */
public final class FileUtil {

	private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

	/**
	 * 获取文件真实名(去掉路径)
	 * 
	 * @param filename
	 * @return
	 */
	public static String getRealFileName(String filename) {
		return FilenameUtils.getName(filename);
	}

	
	/**
	 * 创建文件
	 * @param filePath
	 * @return
	 */
	public static File createFile(String filePath) {
		File file = null;
		try {
			file = new File(filePath);
			File parentDir = file.getParentFile();
			if (!parentDir.exists()) {
				FileUtils.forceMkdir(parentDir);
			}
			
		} catch (Exception e) {
			logger.error(" create file failure", e);
			throw new RuntimeException(e);
		}
		return file;
	}
}
