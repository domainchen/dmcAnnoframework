package com.domainchen.framework.common.util;

import java.net.URLDecoder;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月11日 上午11:38:47
 * @类说明 编码与解码操作工具类
 */
public final class CodecUtil {

	private static final Logger logger = LoggerFactory.getLogger(CodecUtil.class);

	public static String encodeURL(String path) {
		String target = "";
		try {
			target = URLEncoder.encode(path);
		} catch (Exception e) {
			logger.error(" encode url failure", e);
			throw new RuntimeException(e);
		}
		return target;
	}

	public static String decodeURL(String source) {
		String target = "";
		try {
			target = URLDecoder.decode(source);
		} catch (Exception e) {
			logger.error(" encode url failure", e);
			throw new RuntimeException(e);
		}
		return target;
	}
}
