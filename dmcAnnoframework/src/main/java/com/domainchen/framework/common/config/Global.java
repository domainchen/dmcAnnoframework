package com.domainchen.framework.common.config;

import java.util.HashMap;
import java.util.Map;

import com.domainchen.framework.common.util.CastUtil;
import com.domainchen.framework.constant.ConfigConstant;
import com.domainchen.framework.core.util.PropertiesLoader;

/**
 * @author domainchen or cytxiamen@163.com
 * @version 创建时间：2016年3月10日 下午3:44:49
 * @类说明 全局配置类-属性文件助手类
 */
public class Global {
	/**
	 * 当前对象实例
	 */
	private static Global global = new Global();

	/**
	 * 保存全局属性值
	 */
	private static Map<String, String> map = new HashMap<String, String>();

	/**
	 * 属性文件加载对象
	 */
	private static PropertiesLoader propertiesLoader = new PropertiesLoader(ConfigConstant.CONFIG_FILE);

	/**
	 * 获取当前对象实例
	 */
	public static Global getInstance() {
		return global;
	}

	/**
	 * 获取配置
	 */
	public static String getConfig(String key) {
		String value = map.get(key);
		if (value == null) {
			value = propertiesLoader.getProperty(key);
			map.put(key, value);
		}
		return value;
	}

	///////////////////////////////////////////////////////////////////////////////////
	
	public static String getJdbcDriver() {
		return getConfig(ConfigConstant.JDBC_DRIVER);
	}
	
	public static String getJdbcUrl() {
		return getConfig(ConfigConstant.JDBC_URL);
	}
	
	
	public static String getJdbcUserName() {
		return getConfig(ConfigConstant.JDBC_USERNAME);
	}
	
	
	public static String getJdbcPassword() {
		return getConfig(ConfigConstant.JDBC_PASSWORD);
	}
	
	
	public static String getAppBasePackage() {
		return getConfig(ConfigConstant.APP_BASE_PACKAGE);
	}
	
	
	public static String getAppDefaultPath() {
		return getConfig(ConfigConstant.APP_DEFAULT_PATH);
	}
	
	public static String getAppJspPath() {
		return getConfig(ConfigConstant.APP_JSP_PATH);
	}
	
	
	public static String getAppAssertPath() {
		return getConfig(ConfigConstant.APP_ASSERT_PATH);
	}
	
	public static int getAppUploadLimit() {
		return CastUtil.castInt(getConfig(ConfigConstant.APP_UPLOAD_LIMIT),10);
	}
}
